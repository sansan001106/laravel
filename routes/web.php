<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('corona', function() {
	$positif = Http::get('https://api.kawalcorona.com/positif');
	$sembuh = Http::get('https://api.kawalcorona.com/sembuh');
	$meninggal = Http::get('https://api.kawalcorona.com/meninggal');
    return view('corona', compact('positif', 'sembuh', 'meninggal'));
});

Route::resource('post', 'PostController');