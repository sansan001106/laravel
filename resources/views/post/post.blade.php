<table border="1">
	<tr>
		<th>Post</th>
	</tr>
	@foreach ($post as $p)
		<tr>
			<td>{{ $p->post }}</td>
			<td><a href="{{ route('post.edit',[$p->id]) }}">Edit</a></td>
			<td>
				<form method="POST" action="{{ route('post.destroy', [$p->id]) }}">
					@csrf()
					@method('delete')
					<button class="btn btn-danger">Hapus</button>
				</form>
			</td>
		</tr>
	@endforeach
</table>

<a href="{{ route('post.create') }}">Tambah</a>
